package main

import (
	"net"
	"time"

	log "github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"

	"gitlab.com/mergetb/tech/gobble/pkg"
	"gitlab.com/mergetb/tech/rtnl"
)

func main() {

	//log.SetLevel(log.TraceLevel)

	gobble.EnsureTableRules()

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.Fatal(err)
	}

	eth1, err := rtnl.GetLink(ctx, "eth1")
	if err != nil {
		log.Fatal(err)
	}

	vtep666 := rtnl.Link{
		Info: &rtnl.LinkInfo{
			Name: "vtep666",
			Vxlan: &rtnl.Vxlan{
				Vni:  666,
				Link: uint32(eth1.Msg.Index),
			},
		},
	}

	err = vtep666.Present(ctx)
	if err != nil {
		log.Fatal(err)
	}

	// add peer neighbor
	peer := gobble.Neighbor{
		Dst:    net.ParseIP(gobble.PeerGw),
		If:     uint32(vtep666.Msg.Index),
		Mac:    net.HardwareAddr{0x00, 0x00, 0x00, 0x00, 0x99, 0x01},
		Family: unix.AF_INET,
	}
	err = gobble.AddNeighbors([]gobble.Neighbor{peer})
	if err != nil {
		log.Fatal(err)
	}

	nbrs := []gobble.Neighbor{
		{
			Mac:    net.HardwareAddr{0x00, 0x00, 0x00, 0x00, 0x00, 0x01},
			If:     uint32(vtep666.Msg.Index),
			Dst:    net.IPv4(10, 99, 0, 2),
			Family: unix.AF_BRIDGE,
		},
		{
			Mac:    net.HardwareAddr{0x00, 0x00, 0x00, 0x00, 0x00, 0x02},
			If:     uint32(vtep666.Msg.Index),
			Dst:    net.IPv4(10, 99, 0, 2),
			Family: unix.AF_BRIDGE,
		},
		{
			Mac:    net.HardwareAddr{0x00, 0x00, 0x00, 0x00, 0x00, 0x03},
			If:     uint32(vtep666.Msg.Index),
			Dst:    net.IPv4(10, 99, 0, 2),
			Family: unix.AF_BRIDGE,
		},
		{
			Mac:    net.HardwareAddr{0x00, 0x00, 0x00, 0x00, 0x00, 0x04},
			If:     uint32(vtep666.Msg.Index),
			Dst:    net.IPv4(10, 99, 0, 2),
			Family: unix.AF_BRIDGE,
		},
		{
			Mac:    net.HardwareAddr{0x00, 0x00, 0x00, 0x00, 0x00, 0x05},
			If:     uint32(vtep666.Msg.Index),
			Dst:    net.IPv4(10, 99, 0, 2),
			Family: unix.AF_BRIDGE,
		},
		{
			Mac:    net.HardwareAddr{0x00, 0x00, 0x00, 0x00, 0x00, 0x06},
			If:     uint32(vtep666.Msg.Index),
			Dst:    net.IPv4(10, 99, 0, 2),
			Family: unix.AF_BRIDGE,
		},
	}

	rte := gobble.Route{
		Dest:    net.IPv4(10, 99, 0, 2),
		Gateway: net.ParseIP(gobble.PeerGw),
		Oif:     4, //eth2
	}

	for {

		err = gobble.AddRoutes([]gobble.Route{rte})
		if err != nil {
			log.Print(err)
		}
		//time.Sleep(10 * time.Millisecond)
		err = gobble.AddNeighbors(nbrs)
		if err != nil {
			log.Print(err)
		}
		//time.Sleep(10 * time.Millisecond)

		err = gobble.RemoveRoutes([]gobble.Route{rte})
		if err != nil {
			log.Print(err)
		}
		//time.Sleep(10 * time.Millisecond)
		err = gobble.RemoveNeighbors(nbrs)
		if err != nil {
			log.Print(err)
		}
		time.Sleep(10 * time.Millisecond)

	}

}
