#!/bin/bash

protoc -I . monkey.proto --go_out=plugins=grpc:.
go build -o monkeyd monkeyd.go monkey.pb.go
go build -o monkeyc monkeyc.go monkey.pb.go
