package main

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"
)

type svc struct{}

func main() {
	server := grpc.NewServer()
	RegisterMonkeyServer(server, &svc{})

	l, err := net.Listen("tcp", "0.0.0.0:666")
	if err != nil {
		log.Fatal("failed to listen: %#v", err)
	}

	log.Printf("Listening on tcp://%s", "0.0.0.0:666")
	server.Serve(l)
}

func (s *svc) Ping(ctx context.Context, e *Empty) (*Empty, error) {

	return &Empty{}, nil

}
