package main

import (
	"context"
	"log"
	"net"
	"time"

	"golang.org/x/sys/unix"
	"google.golang.org/grpc"

	"gitlab.com/mergetb/tech/cogs/pkg"
	"gitlab.com/mergetb/tech/rtnl"
)

func main() {

	go churn()

	for {

		conn, err := grpc.Dial("10.0.0.2:666", grpc.WithInsecure())
		if err != nil {
			log.Print("dial error: %v", err)
		}
		client := NewMonkeyClient(conn)

		_, err = client.Ping(context.TODO(), &Empty{})
		if err != nil {
			log.Print(err)
		}
		log.Printf("ok")
		conn.Close()

		time.Sleep(5 * time.Millisecond)

	}
}

func churn() {

	for {

		netChurn()
		//routeChurn()
		//vethChurn()

	}

}

const mzid = "lucifer"

func vethChurn() {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.Fatal(err)
	}
	defer ctx.Close()

	ctx.Target, err = rtnl.OpenContext("other")
	if err != nil {
		log.Fatal(err)
	}

	a := &rtnl.Link{
		Info: &rtnl.LinkInfo{
			Name: "a666",
			Ns:   uint32(ctx.Target.Fd()),
			Veth: &rtnl.Veth{
				Peer: "b666",
			},
		},
	}
	err = a.Present(ctx)
	if err != nil {
		log.Fatalf("present: %v", err)
	}

	time.Sleep(100 * time.Millisecond)

	b, err := rtnl.GetLink(ctx, "b666")
	if err != nil {
		log.Fatal("get: %v", err)
	}

	err = b.Absent(ctx)
	if err != nil {
		log.Fatal("absent: %v", err)
	}

}

func routeChurn() {

	ctx, err := rtnl.OpenContext("other")
	if err != nil {
		log.Fatal(err)
	}
	defer ctx.Close()

	eth0, err := rtnl.GetLink(ctx, "eth0")
	if err != nil {
		log.Fatal(err)
	}

	drt := &rtnl.Route{
		Hdr:  unix.RtMsg{Dst_len: 24},
		Dest: net.IPv4(10, 3, 0, 0),
		Oif:  uint32(eth0.Msg.Index),
	}
	err = drt.Present(ctx)
	if err != nil {
		log.Fatal(err)
	}

	rte := &rtnl.Route{
		Hdr:     unix.RtMsg{Dst_len: 0},
		Dest:    net.IPv4(0, 0, 0, 0),
		Gateway: net.IPv4(10, 2, 0, 99),
	}
	err = rte.Present(ctx)
	if err != nil {
		log.Fatal(err)
	}

	time.Sleep(10 * time.Millisecond)

	err = rte.Absent(ctx)
	if err != nil {
		log.Fatal(err)
	}

	err = drt.Absent(ctx)
	if err != nil {
		log.Fatal(err)
	}

	time.Sleep(10 * time.Millisecond)

}

func netChurn() {

	err := cogs.CreateInfrapodNS(mzid)
	if err != nil {
		log.Print(err)
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.Fatal(err)
	}
	defer ctx.Close()

	spec := &cogs.EnclaveSpec{
		Vindex:         666,
		ExternalSubnet: "1.2.3.4/24",
		Interfaces: []*cogs.EnclaveVeth{
			{
				Inner:         "ceth0",
				Outer:         "ifr666",
				Vni:           666,
				Vid:           666,
				Vindex:        666,
				EvpnAdvertise: false,
				Bridge:        "mzbr666",
				Address:       "172.30.0.66/24",
			},
			{
				Inner:   "ceth1",
				Outer:   "svc666",
				Bridge:  "svcbr",
				Address: "172.31.0.66/24",
			},
		},
	}

	err = cogs.EnclaveBridge(spec, ctx)
	if err != nil {
		log.Print(err)
	}

	err = cogs.EnclaveIptables(mzid, spec)
	if err != nil {
		log.Print(err)
	}

	for _, ifx := range spec.Interfaces {

		err = cogs.CreateIfx(mzid, ifx, false)
		if err != nil {
			log.Print(err)
		}

	}

	time.Sleep(100 * time.Millisecond)

	for _, ifx := range spec.Interfaces {

		err = cogs.DeleteIfx(mzid, ifx)
		if err != nil {
			log.Print(err)
		}

	}

	err = cogs.DelEnclaveIptables(mzid, spec)
	if err != nil {
		log.Print(err)
	}

	err = cogs.DelEnclaveBridge(spec, ctx)
	if err != nil {
		log.Print(err)
	}

	err = cogs.DeleteInfrapodNS(mzid)
	if err != nil {
		log.Print(err)
	}

	time.Sleep(100 * time.Millisecond)

}
