#!/bin/bash

sudo ip addr del 10.99.0.1/32 dev lo

sudo ip link set down dev eth1
sudo ip addr del 10.0.0.1/24 dev eth1

sudo ip link set down dev eth2
sudo ip link del vtep666

ip -br -c addr
