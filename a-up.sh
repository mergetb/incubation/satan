#!/bin/bash
sudo ip addr add 10.99.0.1/32 dev lo

sudo ip addr add 10.0.0.1/24 dev eth1
sudo ip link set up dev eth1

sudo ip link set up dev eth2

sudo ip link add vtep666 type vxlan id 666 dev eth2 dstport 4789
sudo ip link set up dev vtep666

sudo ip netns add other
sudo ip netns exec other ip link add eth0 type veth peer oeth0
sudo ip netns exec other ip link set up dev eth0
sudo ip netns exec other ip link set netns 1 dev oeth0
sudo ip netns exec other ip addr add 10.2.0.1/24 dev eth0
sudo ip link set up oeth0

sudo ip link add svcbr type bridge
sudo ip link set up dev svcbr

ip -br -c addr
