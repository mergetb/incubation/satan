module satan

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/json-iterator/go v1.1.6 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/mergetb/tech/cogs v0.2.19-0.20190712163417-ee3ad892444b
	gitlab.com/mergetb/tech/gobble v0.1.2
	gitlab.com/mergetb/tech/rtnl v0.1.7
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7
	golang.org/x/sys v0.0.0-20190710143415-6ec70d6a5542
	google.golang.org/grpc v1.22.0
)

