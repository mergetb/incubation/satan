#!/bin/bash

sudo ip link set down dev eth1
sudo ip addr del 10.0.0.2/24 dev eth1

sudo ip link set down dev eth2

ip -br -c addr
