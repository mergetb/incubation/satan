topo = {
  name: 'satan',
  nodes: [deb('a'), deb('b')],
  links: [
    Link('a', 1, 'b', 1),
    Link('a', 2, 'b', 2)
  ]
}

function deb(name) {
  return {
    name: name,
    image: 'debian-buster',
    memory: { capacity: GB(2) },
    cpu: { cores: 4 },
    mounts: [{source: env.PWD, point: '/tmp/satan'}],
  }
}
